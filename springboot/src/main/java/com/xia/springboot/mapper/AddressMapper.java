package com.xia.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xia.springboot.entity.Address;

/**
 * @Author: Xia
 * @Date: 2024/3/18  16:30
 */
public interface AddressMapper extends BaseMapper<Address> {
}
